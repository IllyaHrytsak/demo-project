$(document).ready(function () {

    $("#transfer-form").submit(function (event) {

        event.preventDefault();

        fire_ajax_submit();

    });

});

function fire_ajax_submit() {

    var inputString = {};
    inputString["input"] = $("#input-text").val();

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/transfer-text",
        data: JSON.stringify(inputString),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            var info = "";
            var unique = 0;
            for (var key in data) {
                if (data.hasOwnProperty(key)){
                    info += key + " - " + data[key] + "\n";
                    unique++;
                }
            }
            info += "\nUnique: " + unique;
            $('#output-text').html(info);

        },
        error: function (error) {
            if (error.hasOwnProperty("responseText")) {
                var response = JSON.parse(error.responseText);
                if (response.hasOwnProperty("message"))
                    $('#output-text').html(response.message);
            } else {
                $('#output-text').html("Unknown error");
            }
        }
    });

}