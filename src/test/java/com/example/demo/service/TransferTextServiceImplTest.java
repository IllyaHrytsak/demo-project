package com.example.demo.service;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;


public class TransferTextServiceImplTest {

    private TransferTextService transferTextService;

    @Before
    public void setUp() throws Exception {
        transferTextService = new TransferTextServiceImpl();
    }

    @Test
    public void inputTwoWordTest() {
        String inputString = "Hello Test";
        Map<String, Integer> expectedResult = new HashMap<>();
        expectedResult.put("Hello", 1);
        expectedResult.put("Test", 1);
        Map<String, Integer> actualResult = transferTextService.transferText(inputString);
        assertEquals(expectedResult ,actualResult);
    }

    @Test
    public void inputTwoDuplicateWordsTest() {
        String inputString = "Hello Hello";
        Map<String, Integer> expectedResult = new HashMap<>();
        expectedResult.put("Hello", 2);
        Map<String, Integer> actualResult = transferTextService.transferText(inputString);
        assertEquals(expectedResult ,actualResult);
    }

    @Test
    public void inputOneDuplicateAndTwoUsualWordsTest() {
        String inputString = "Hello Hello Test World";
        Map<String, Integer> expectedResult = new HashMap<>();
        expectedResult.put("Hello", 2);
        expectedResult.put("Test", 1);
        expectedResult.put("World", 1);
        Map<String, Integer> actualResult = transferTextService.transferText(inputString);
        assertEquals(expectedResult ,actualResult);
    }

}